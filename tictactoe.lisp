;; Functions for a tic-tac-toe game
;;

;; Example board - represented as a list of nine markers on the 
;; tic-tac-toe board

(defparameter *emptyboard* (list '- '- '- '- '- '- '- '- '-))
(defparameter *the-board* (list 'o 'x 'o 'o 'o 'o 'x 'x 'x))
(defparameter *turn* 'o)

;; Prints a board that is a list of nine symbols in row-major order
;;
(defun print-board (board)
  (format t "=============")
  (do ( (i 0 (+ i 1)))
      ( (= i 9) 'done)
      ;; all these expressions are evaled in order every iteration
      (if ( = (mod i 3) 0)  (format t "~%|") nil)
      (format t " ~A |" (nth i board))
  )
  (format t "~%=============")
  )


;; Clears a given board by setting all values to -
(defun clear-board ()
  (make-list 9 :initial-element '-))
    

;; Determines if a list representing a row or column or diagonal
;; of a tic-tac-toe board is a victory.
(defun victory (alist) 
  (and
   (and (equal (first alist) (second alist))
        (equal (second alist) (third alist)))
   
        (or (string-equal "x" (first alist))
            (string-equal "o" (first alist)))))

;; Returns a list consisting of the nth row (zero-based) of
;; a tic-tac-toe board
(defun grab-row (board row)
  (let ((x (* 3 row)))
    (list (nth x board)
          (nth (+ x 1) board)
          (nth (+ x 2) board))
  )
)

;; Returns a list consisting of the nth column (zero-based) of
;; a tic-tac-toe board
(defun grab-col (board col)
  (list (nth col board)
        (nth (+ col 3) board)
        (nth (+ col 6) board)))


;; Returns a list consisting of the left diagonal of
;; a tic-tac-toe board
(defun grab-left-diag (board)
  (list (nth 0 board)
        (nth 4 board)
        (nth 8 board)))

;; Returns a list consisting of the right diagonal of
;; a tic-tac-toe board
(defun grab-right-diag (board)
  (list (nth 2 board)
        (nth 4 board)
        (nth 6 board)))

;; Given a move and a current board, checks if the move is valid.
(defun check-valid (position board)
  (and (and (> position -1) (< position 9))
       (string-equal "-" (nth position board))))


;; Given a board, a player (x or o), and a position(0-8),
;; places a move on the board
(defun place-move (board player position)
  (if (eq (check-valid position board) NIL)
      (progn
        (format t "~%Invalid move, try again")
        (play (set-turn player) board))
    (let ((new-board (copy-list board)))
      (setf (nth position new-board) player)
      new-board)))

;; Given a position and a board, this function returns the row the position is in.
(defun get-row (board position)
  (if (= (mod position 3) 0)
      (grab-row board position))
  (if (= (mod position 3) 1)
      (grab-row board (- position 1)))

  (if (= (mod position 3) 2)
      (grab-row board (- position 2))))

;; Given a position and a board, determine the row that the position is in.
(defun get-col (board position)
  (if (= (mod position 3) 0)
      (grab-col board 0) nil)
  (if (= (mod position 3) 1)
      (grab-col board 1) nil)
  (if (= (mod position 3) 2)
      (grab-col board 2) nil))

(defun check-full (board)
  (find "-" board))
 
;; Given a board, determine if it is in a winning state
(defun check-win (board)
  (or (eq (victory (grab-row board 1)) T)
      (eq (victory (grab-row board 2)) T)
      (eq (victory (grab-row board 3)) T)
      (eq (victory (grab-col board 0)) T)
      (eq (victory (grab-col board 1)) T)
      (eq (victory (grab-col board 2)) T)
      (eq (victory (grab-left-diag board)) T)
      (eq (victory (grab-right-diag board)) T)
      ))


;; Given a board, generate a move for the AI to pick
(defun gen-move (board)
  (loop for x in '(8 6 0 2 7 3 5 1 4)
        until (eq (check-valid x board) T)
        collect x
        finally
        (return x)))

;; Given a player, sets the turn to the opposite persons turn
(defun set-turn (player)
  (if (eq player 'x) 'o 'x))

;; Call this function to start playing the game
(defun start ()
  (play 'o (clear-board)))


;; Keeps playing the game until a win state has been met
(defun play (player board)
  (loop while  (not (eq (check-win board) T))
        do (setf player (set-turn player))
        do (format t "~%~%||||||||||||||||||||||||||||||||~%~%<#---- Player ~A's turn ----#>~%~%" player)
        do (print-board board)
        do (format t "~%~%What move would you like to make?:")
        do (if (string-equal "x" player)
               (let ((position (gen-move board)))
                 (setf board (place-move board player position)))
        
               (let ((position (parse-integer(read-line))))
             (setf board (place-move board player position))))
  do (format t "~%~%||||||||||||||||||||||||||||||||~%~%"))
        (format t "~%~%#=== Player ~A Wins! ===#~%~%" player)
        (print-board board));; Print the board and say that the player wins

